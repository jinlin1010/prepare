package number;

public class ConsecutiveNumbersSum {

	public static void main(String[] args) {
		ConsecutiveNumbersSum c = new ConsecutiveNumbersSum();
		System.out.println(c.consecutiveNumbersSum(333764327));
	}

	// sliding window, Time Limit Exceeded
	public int consecutiveNumbersSum(int N) {
		if (N < 3)
			return 1;
		int res = 1;
		int start = 1;
		int end = 1;
		int endSum = 0;
		while (end <= (N / 2 + 1)) {
			endSum += end;
			while (endSum > N) {
				endSum -= start;
				start++;
			}
			if (endSum == N) {
				res++;
			}
			end++;
		}
		return res;
	}
}
