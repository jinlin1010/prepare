package dp;

public class IntegerBreak {
    public static void main(String [] args) {
    	IntegerBreak i = new IntegerBreak();
    	System.out.println(i.integerBreak(10));
    }
    
    public int integerBreak(int n) {
        if(n<=3) return n-1;
        int [] dp = new int[n+1];
          return integerBreak(n, dp);
      }
      
      public int integerBreak(int n, int [] dp) {
    	  
          if(n == 0 || n == 1) return 1;
          if(dp[n] != 0) {
        	  System.out.println(dp[n]);
        	  return dp[n];
          }
          int max = 0;
          for(int i=1; i<=9; i++) {
              if(n-i >=0) {
                  max = Math.max(max, integerBreak(n-i, dp)*i);
              } else {
                  continue;
              }
          }
          dp[n] = max;
          return max;
      }
      
      public int integerBreak2(int n) {
    	  if(n == 0 || n == 1) return 1;
    	  int [] dp = new int [n+1];
    	  int i = n;
    	  while(i > 2) {
    		  for(int j=1; j<=9; j++) {
    			  if(i-j >=0) {
    			    dp[i] = Math.max(dp[i], dp[i-j]*j);
    			    i = i -j;  
    			  } else {
    				  continue;
    			  }
    			  
    		  }
    	  }
    	  return dp[0];
      }
}
