package dp;

public class CountSortedVowelStrings {
	public static void main(String[] args) {
		CountSortedVowelStrings c = new CountSortedVowelStrings();
		System.out.println(c.countVowelStrings(3));
	}

	public int countVowelStrings(int n) {
		if (n == 1)
			return 5;
		int dp[][] = new int[n + 1][5];
		dp[1][0] = 1;
		dp[1][1] = 2;
		dp[1][2] = 3;
		dp[1][3] = 4;
		dp[1][4] = 5;
		for (int i = 2; i <= n; i++) {
			dp[i][0] = 1;
			dp[i][1] = dp[i][0] + dp[i - 1][1];
			dp[i][2] = dp[i][1] + dp[i - 1][2];
			dp[i][3] = dp[i][2] + dp[i - 1][3];
			dp[i][4] = dp[i][3] + dp[i - 1][4];
		}

		return dp[n][4];

	}
}
