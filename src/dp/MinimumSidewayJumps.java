package dp;

import java.util.Arrays;

import util.Print;

public class MinimumSidewayJumps {
	public static void main(String[] args) {
		MinimumSidewayJumps m = new MinimumSidewayJumps();
		int[] obstacles = { 0, 1, 2, 3, 0 };
		System.out.println(m.minSideJumps1(obstacles));

	}

	private int solve(int i, int lane, int n, int[] obstacles, int[][] dp) {
		if ((i < n - 1 && obstacles[i] == lane) || (i > 0 && obstacles[i - 1] == lane))
			return 1000000;
		if (i == n - 1)
			return 0;
		if (dp[i][lane] != -1)
			return dp[i][lane];
		int a = solve(i + 1, lane, n, obstacles, dp);
		int b = 0, c = 0;
		if (lane == 1) {
			b = 1 + solve(i + 1, 2, n, obstacles, dp);
			c = 1 + solve(i + 1, 3, n, obstacles, dp);
		} else if (lane == 2) {
			b = 1 + solve(i + 1, 1, n, obstacles, dp);
			c = 1 + solve(i + 1, 3, n, obstacles, dp);
		} else {
			b = 1 + solve(i + 1, 1, n, obstacles, dp);
			c = 1 + solve(i + 1, 2, n, obstacles, dp);
		}
		System.out.println("i: " + i + " lane: " + lane + " a: " + a + " b: " + b + " c: " + c);
		return dp[i][lane] = Math.min(a, Math.min(b, c));
	}

	public int minSideJumps(int[] obstacles) {
		int n = obstacles.length;
		int[][] dp = new int[n + 1][4];
		for (int[] arr : dp)
			Arrays.fill(arr, -1);
		int result = solve(0, 2, n, obstacles, dp);
		Print.printDoubleArray(dp);
		return result;
	}

	public int minSideJumps1(int[] obstacles) {
		if (obstacles == null || obstacles.length == 0)
			return 0;
		int[][] dp = new int[obstacles.length][3];
		dp[0][0] = 1;
		dp[0][1] = 0;
		dp[0][2] = 1;
		for (int i = 1; i < obstacles.length; i++) {
			int min = Integer.MAX_VALUE;
			for (int j = 0; j < 3; j++) {
				if (obstacles[i] == j + 1) {
					dp[i][j] = Integer.MAX_VALUE;
				} else {
					dp[i][j] = dp[i-1][j];
				}
				min = Math.min(min, dp[i][j]);
			}
			for(int j=0; j<3; j++) {
				if(j+1 != obstacles[i] && min != dp[i][j]) {
					dp[i][j] = min + 1;
				}
			}
	
		}
		int result = Integer.MAX_VALUE;
		for(int i=0; i<3; i++) {
			result = Math.min(result, dp[obstacles.length-1][i]);
		}

		return result;
	}
}
