package dp;

import util.Print;

public class OnesAndZeros {
	public static void main(String[] args) {
		int[] weight = { 1, 2, 5, 6, 7 };
		int[] value = { 1, 6, 18, 22, 28 };
		OnesAndZeros o = new OnesAndZeros();
		
		System.out.println(o.maxValue(weight, value, 11));
	    	
	}
	
	public int maxValue(int [] weight, int [] value, int totalWeight) {
		
		int n = weight.length;
		int [][] dp = new int[n+1][totalWeight+1];
		for(int i=0; i<= totalWeight; i++) {
		  	dp[0][i] = 0;
		}
		for(int i=1; i<= n; i++) {
			dp[i][0] = 0;
		}
		
		for(int i=1; i<=n; i++) {
			for(int j=1; j<=totalWeight; j++) {
				if(weight[i-1] > j) {
					dp[i][j] = dp[i-1][j];
				} else {
					dp[i][j] = Math.max(dp[i-1][j], value[i-1] + dp[i-1][j-weight[i-1]]);
				}
			}
		}
		Print.printDoubleArray(dp);
		
		return dp[n][totalWeight];
	}
	
}
