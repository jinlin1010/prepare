package dp;

public class NumSubArrayProductLessThanK {
	public static void main(String args[]) {
		int[] nums = { 10, 9, 10, 4, 3, 8, 3, 3, 6, 2, 10, 10, 9, 3 };

		NumSubArrayProductLessThanK n = new NumSubArrayProductLessThanK();
		System.out.print(n.numSubarrayProductLessThanK(nums, 19));
	}

	public int numSubarrayProductLessThanK(int[] nums, int k) {
		if (nums == null || nums.length == 0 || k == 0)
			return 0;
		int res = 0;
		if (nums[0] < k)
			res = 1;
		for (int i = 1; i < nums.length; i++) {
			int cnt = 0;
			int tmp = 1;
			for (int j = i; j >= 0; j--) {
				tmp = tmp * nums[j];
				if (tmp < k) {
					cnt++;
					
				} else {
					break;
				}
			}
			res = res + cnt;
			System.out.println(i + 1 +  " - "+ res);
		}
		return res;
	}
}
