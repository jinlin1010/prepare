package dp;

public class NumDecodings {

	public static void main(String[] args) {
		NumDecodings n = new NumDecodings();
		System.out.println(n.numDecodings("2101"));
	}

	public int numDecodings(String s) {
		Integer[] dp = new Integer[s.length() + 1];
		return dfs(s, s.length(), dp);
	}

	public int dfs(String s, int n, Integer[] dp) {
		if (n == 0)
			return 1;
		if (n == 1) {
			return s.charAt(0) == '0' ? 0 : 1;
		}
		if (dp[n] != null) {
			return dp[n];
		}
		int res = 0;
		char x = s.charAt(n - 1);
		char y = s.charAt(n - 2);
		if (x != '0') {
			res += dfs(s, n - 1, dp);
		}
		int yx = (y - '0') * 10 + (x - '0');
		if (yx <= 26 && yx >= 10) {
			res += dfs(s, n - 2, dp);
		}
		dp[n] = res;
		return res;
	}
}
