package dp;

public class TakeAwayTheBottle {
	public static void main(String[] args) {
		int [] arr = {1,3,4,1,5};
		TakeAwayTheBottle t = new TakeAwayTheBottle();
		System.out.println(t.takeAwayTheBottle(arr));

	}

	public int takeAwayTheBottle(int[] arr) {
		int [][] dp = new int [550][550];
		// Write your code here.
		int i, j, k, l;
		int n = arr.length;
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++)
				dp[i][j] = n;
		}

		for (i = 0; i < n; i++) {
			dp[i][i] = 1;
		}
		for (i = 0; i + 1 < n; i++) {
			if (arr[i] == arr[i + 1])
				dp[i][i + 1] = 1;
			else
				dp[i][i + 1] = 2;
		}
		for(int o=0; o<arr.length; o++) {
			for(int m=0; m<arr.length; m++) {
				System.out.print(dp[o][m]);
			}
			System.out.println();
		}
		for (l = 2; l < n; l++) {
			for (i = 0; i + l < n; i++) {
				j = i + l;
				if (arr[i] == arr[j]) {
					dp[i][j] = dp[i + 1][j - 1];
				}
				for (k = i; k < j; k++) {
					dp[i][j] = Math.min(dp[i][j], dp[i][k] + dp[k + 1][j]);
				}
			}
		}
		return dp[0][n - 1];
	}
}
