package dp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BurstBallons {

	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 3, 1, 5, 8 };
		List<Integer> list = new ArrayList<>();
		Collections.addAll(list, arr);
		BurstBallons b = new BurstBallons();
		// System.out.println(b.dfs(list, 0));
		int[] array = new int[] { 3, 1, 5, 8 };
		System.out.println(b.getMax(array));		
	}

	// greedy, wrong approach
	public int dfs(List<Integer> list, int res) {
		if (list.size() == 1) {
			return res + list.get(0);
		}
		int max = Integer.MIN_VALUE;
		int maxIndex = -1;
		for (int i = 0; i < list.size(); i++) {
			int current;
			if (i - 1 < 0) {
				current = list.get(i + 1) * 1;
			} else if (i + 1 >= list.size()) {
				current = list.get(i - 1) * 1;
			} else {
				current = list.get(i - 1) * list.get(i + 1);
			}
			if (current > max) {
				max = current;
				maxIndex = i;
			}
		}
		res = res + max * list.get(maxIndex);
		list.remove(maxIndex);
		return dfs(list, res);

	}
    // dp[L][R] = max(dp[L][R], B[L]*B[LAST]*B[R] + dp[L][last-1] + dp[last+1][R]
	public int getMax(int[] nums) {

        if(nums == null || nums.length == 0) return 0;
        int N = nums.length + 2;
        int [] arr = new int[N];
        arr[0] = 1;
        arr[N-1] = 1;
        for(int i=1; i<= nums.length; i++) {
            arr[i] = nums[i-1];
        }
        int [][] dp = new int [N][N];
        // 1 ~ array length
        for(int len = 1; len <= nums.length;  len++) {
            //  1 ~  reduce base on len
            for(int left=1; left<= nums.length - len + 1; left++){
                int right = left + len - 1;
                // left ~ right
                for(int last = left; last <= right; last++){
                    dp[left][right] = Math.max(dp[left][right], arr[left-1]* arr[last]*arr[right+1] + dp[left][last-1] + dp[last+1][right]);
                }
            }
        }
        
        return dp[1][nums.length];
	}
}
