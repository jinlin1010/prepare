package dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreak {
	public static void main(String[] args) {
		String s = "catsandog";
		List<String> wordDict = new ArrayList<>();
		wordDict.add("cats");
		wordDict.add("dog");
		wordDict.add("sand");
		wordDict.add("cat");
		WordBreak w = new WordBreak();
		System.out.println(w.wordBreak(s, wordDict));
	}
    // put list to hashset,  hashset contains string 
	// dp[i] = true iff dp[i-str.lenth]= true  return dp[len]
	public boolean wordBreak(String s, List<String> wordDict) {
        Set<String> set = new HashSet<>(wordDict);
		boolean [] dp = new boolean [s.length()];
		dp[0] = true;
		int idx = 0;
        for(int i=0; i<s.length(); i++) {
			String tmp = s.substring(idx, i);
			if(set.contains(tmp) && i-idx >= 0 && dp[idx]){
				dp[i] = true;
				idx = i;
			}
		}
		return dp[s.length()-1];
	}
	
    public boolean wordBreak(String s, Set<String> dict) {
        if(s == null || dict == null)
			return false;
		boolean [] dp = new boolean [s.length() + 1];
		dp[0] = true;
		for(int i=0; i<s.length(); i++){
			if(!dp[i]) continue;
			for(String a: dict){
				int end = i + a.length();
				if(end > s.length()){
					continue;
				}
				if(dp[end]) continue;
				if(s.substring(i, end).equals(a)) dp[end]= true;
			}
		}
		return dp[s.length()];
    }


}
