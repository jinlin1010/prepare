package dp;

public class IsSubsequence {
	public static void main (String args[]) {
		IsSubsequence i = new IsSubsequence();
		System.out.println(i.isSubsequence("abc", "rerwerw"));
	}
	
    public boolean isSubsequence(String s, String t) {
        if(s.isEmpty()) return true;
        if(t.isEmpty()) return false;
        int [] dp = new int[t.length()+1];
        for(int i=0; i<t.length(); i++){
            if(dp[i] < s.length() && t.charAt(i) == s.charAt(dp[i])){
                dp[i+1] = dp[i] +1;
            } else {
                dp[i+1] = dp[i];
                
            }
        }
        return dp[dp.length-1] == s.length();
        
    }
}
