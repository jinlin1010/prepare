package dp;

public class WeightCapacity {
	public static void main (String [] args) {
		WeightCapacity w = new WeightCapacity();
		int [] weights = {67356,233900,982590,372879,415998,
						461941,98935,239287,433413,36109,
						887779,857424,928267,49000,321030,
						230551,332083,580018,932293,851765,
						688961,989160,222830,425340,21042,
						386203,878017,246204,194031,837181,
						87247,909684,26518,908975,361664,
						213872,744761,278193,434280,60833};
		int maxCapacity = 237733;
		System.out.println(w.weightCapacity(weights, maxCapacity));
	}
	
    public int weightCapacity(int[] weights, int maxCapacity) {
        // Write your code here
        if(weights == null || weights.length == 0 || maxCapacity == 0) return 0;
        if(weights.length == 1 &&  weights[0] < maxCapacity) return weights[0];
        boolean [] dp = new boolean [maxCapacity+1];
        dp[0] = true;
        for(int i=0; i<weights.length; i++) {
//        		if(weights[i] < maxCapacity) {
//        			dp[weights[i]] = true;
//        		}
        		for(int j=dp.length-1;  j>=weights[i]; j--) {
        			dp[j] = dp[j] || dp[j-weights[i]];
        		}
        }
        int res = 0;
        for(int i=dp.length-1; i>0; i--) {
            if(dp[i]) {
                res = i;
                break;
            }
        }
        return res;

    }
}
