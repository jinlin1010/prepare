package array;

import java.util.Arrays;

import util.Print;

public class maxIncreaseSum {
	public static void main(String args[]) {
		Integer[] arr = { 4, 5, 6, 5 };
		Integer[] arr1 = { 2, 1, 2, 1 };
		maxIncreaseSum m = new maxIncreaseSum();
		m.sortArray(arr, arr1);
		System.out.println(m.findMaxIncreaseSum(arr));
	}

	public int findMaxIncreaseSum(Integer[] arr) {
		int[] tmpArr = new int[arr.length];
		tmpArr[0] = arr[0];
		int max = tmpArr[0];
		for (int i = 1; i < arr.length; i++) {
			tmpArr[i] = arr[i];
			for (int j = 0; j < i; j++) {
				if (arr[j] <= arr[i]) {
					// find the max of previous index
					tmpArr[i] = Math.max(tmpArr[i], tmpArr[j] + arr[i]);
				}
			}
			max = Math.max(tmpArr[i], max); // keep max of the temp array

		}
		Print.printArray(tmpArr);
		return max;
	}

	public int sortArray(Integer [] arr1, Integer [] arr2) {
		Arrays.sort(arr1, (a, b) -> a - b);
		
		return -1;
	}
}
