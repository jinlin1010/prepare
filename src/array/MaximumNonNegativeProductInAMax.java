package array;

public class MaximumNonNegativeProductInAMax {
	public static void main(String[] args) {
		int[][] grid = new int[][] { { 1, -1, 2, 1, -1, 0, 0, 4, 3, 2, 0, -2, -2 },
				{ -2, 3, 3, -1, -1, 0, 0, -2, 4, -3, 3, 0, 0 }, { -4, -1, -1, -2, 2, -1, -2, -2, 0, 3, -1, -4, 1 },
				{ -3, 4, -3, 0, -3, 1, -3, 1, 4, 4, -4, -4, -2 }, { 3, -3, 1, 0, -1, -4, -4, -4, 3, 2, 2, 3, 3 },
				{ 2, -1, -1, -4, -3, -3, 4, 2, 3, 4, 4, -4, 0 }, { 4, -1, 2, -3, -1, -1, -3, -4, 4, 4, 4, -3, -1 },
				{ -3, -4, 4, -2, -1, 2, 3, -1, 2, 3, 4, 4, -4 }, { -3, -1, -2, 1, 1, -1, -3, -4, -3, 1, -3, 3, -4 },
				{ 2, 4, 4, 4, -3, -3, 1, -1, 3, 4, -1, 1, 4 }, { 2, -2, 0, 4, -1, 0, -2, 4, -4, 0, 0, 2, -3 },
				{ 1, 1, -3, 0, -4, -4, -4, -4, 0, -1, -4, -1, 0 }, { 3, -1, -3, -3, -3, -2, -1, 4, -1, -2, 4, 2, 3 } };

		MaximumNonNegativeProductInAMax m = new MaximumNonNegativeProductInAMax();
		System.out.println(m.maxProductPath(grid));
	}

	class Obj {
		long max;
		long min;

		Obj(long max, long min) {
			this.max = max;
			this.min = min;
		}

	}

	public int maxProductPath(int[][] grid) {
		int m = grid.length;
		int n = grid[0].length;
		Obj[][] dp = new Obj[m][n];
		dp[0][0] = new Obj(grid[0][0], grid[0][0]);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0 && j == 0)
					continue;
				long upMax, upMin, leftMax, leftMin;
				upMax = leftMax = Long.MIN_VALUE;
				upMin = leftMin = Long.MAX_VALUE;

				if (i > 0) {
					leftMax = Math.max(grid[i][j] * dp[i - 1][j].max, grid[i][j] * dp[i - 1][j].min);
					leftMin = Math.min(grid[i][j] * dp[i - 1][j].max, grid[i][j] * dp[i - 1][j].min);
				}
				if (j > 0) {

					upMax = Math.max(grid[i][j] * dp[i][j - 1].max, grid[i][j] * dp[i][j - 1].min);
					upMin = Math.min(grid[i][j] * dp[i][j - 1].max, grid[i][j] * dp[i][j - 1].min);
				}
				dp[i][j] = new Obj(Math.max(upMax, leftMax), Math.min(upMin, leftMin));

				print(dp);
			}
		}
		if (dp[m - 1][n - 1].max < 0) {
			return -1;
		} else {
			return (int) (dp[m - 1][n - 1].max % 1000000007);
		}
	}

	public static void print(Obj[][] obj) {
		for (int i = 0; i < obj.length; i++) {
			for (int j = 0; j < obj.length; j++) {
				if (obj[i][j] == null)
					System.out.print("0,0|");
				else
					System.out.print(obj[i][j].max + "," + obj[i][j].min + "|");
			}
			System.out.println();
		}
	}
}
