package array;

public class Stock {

	public static void main(String args[]) {
		int[] arr = { 3, 2, 6, 5, 0, 3 };
		Stock s = new Stock();
		System.out.println(s.maxProfit2(arr, 2));
	}

	public int maxProfit(int[] prices, int k) {
		int len = prices.length;
		int[][] local = new int[len][k + 1];
		int[][] global = new int[len][k + 1];
		for (int i = 1; i < len; i++) {
			int diff = prices[i] - prices[i - 1];
			for (int j = 1; j < k + 1; j++) {
				local[i][j] = Math.max(global[i - 1][j - 1] + Math.max(diff, 0), local[i - 1][j] + diff);
				global[i][j] = Math.max(local[i][j], global[i - 1][j]);
			}
		}
		return global[len - 1][k];
	}

    // local(j) = max (global(j-1) + 正相邻差 , local(j) + 相邻差）
	// global(j) = max (local(j) , global(j))
	public int maxProfit2(int[] prices, int k) {
		int len = prices.length;
		int [] local = new int [k+1];
		int [] global = new int [k+1];
		for (int i=1; i< len; i++) {
			int diff = prices[i] - prices[i-1];
			for (int j=k; j>=1; j--) {
				local[j] = Math.max(global[j-1] + Math.max(diff, 0), local[j] + diff);
				global[j] = Math.max(local[j], global[j]);
			}
		}
		return global[k];
	}

}
