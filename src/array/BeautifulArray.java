package array;

import java.util.ArrayList;

import util.Print;

public class BeautifulArray {

	// A1 = A * 2 - 1 is beautiful with only odds from 1 to N * 2 -1
	// A2 = A * 2 is beautiful with only even from 2 to N * 2
	// B = A1 + A2
	public static void main(String[] args) {
		BeautifulArray b = new BeautifulArray();
		b.beautifulArray(10);

	}

	public int[] beautifulArray(int N) {
		ArrayList<Integer> res = new ArrayList<>();
		res.add(1);
		while (res.size() < N) {
			ArrayList<Integer> tmp = new ArrayList<>();
			for (int i : res) {
				if (i * 2 - 1 <= N)
					tmp.add(i * 2 - 1);
			}
			for (int i : res)
				if (i * 2 <= N)
					tmp.add(i * 2);
			Print.printIntegerList(tmp);
			System.out.println();
			res = tmp;
		}
		return res.stream().mapToInt(i -> i).toArray();
	}

}
