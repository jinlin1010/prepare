package array;

import java.util.ArrayList;
import java.util.List;

import util.Print;

public class SummaryRange {

	public static void main(String[] args) {
		int[] arr = { 0, 1, 2, 4, 5, 7 };
		SummaryRange sr = new SummaryRange();
		Print.printStringList(sr.summaryRanges(arr));
	}

    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        if(nums.length == 0)  return res;
        int cur = nums[0];
        int move = nums[0];
        for(int i=0; i<nums.length; i++) {
          if (i < nums.length-1 && nums[i+1] - nums[i] == 1) {
              move = nums[i+1];
          } else {
              if(cur == move) {
                  res.add(cur + "");
              } else {
                  res.add(cur + "->" + move);
              }
              if(i< nums.length -1) {
                cur = nums[i+1];
                move = nums[i+1];
              }
          }  
        }
        return res;
    }

}
