package array;

import java.util.HashMap;
import java.util.Map;

public class FindNumSlice {
	public static void main(String args[]) {
		int[] arr = { 1, 1, 1, 1, 2, 2, 2, 2, 2, 2 };
		FindNumSlice f = new FindNumSlice();
		System.out.println(f.findNumSlice(arr));
		System.out.println(f.findNumSlice2(new int[] { 1, 2, 3, 4, 5 }));
	}

	public int findNumSlice(int[] arr) {
		int cnt, num, res;
		cnt = num = res = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] == arr[i - 1]) {
				cnt++;
				if (cnt > 1) {
					num += cnt - 1;
				}
			} else {
				res += num;
				num = 0;
				cnt = 0;
			}
		}
		res += num;

		return res;
	}

	public int findNumSlice2(int[] arr) {
		int res = 0;
		Map<Integer, Integer>[] mapArr = new Map[arr.length];
		for (int i = 0; i < arr.length; i++) {
			mapArr[i] = new HashMap<>(i);
			for (int j = 0; j < i; j++) {
				int diff = arr[i] - arr[j];

				int sum = mapArr[j].getOrDefault(diff, 0);
				int origin = mapArr[i].getOrDefault(diff, 0);
				mapArr[i].put(diff, sum + origin + 1);
				res += sum;

			}
		}

		return res;
	}
}
