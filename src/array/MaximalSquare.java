package array;

public class MaximalSquare {
	public static void main(String[] args) {
		char [][] m = { 
				{ '1', '0', '1', '0', '0' }, 
				{ '1', '0', '1', '1', '1' }, 
				{ '1', '1', '1', '1', '1' }, 
				{ '1', '0', '1', '1', '1' }, };
		MaximalSquare ms = new MaximalSquare();
		System.out.print(ms.maximalSquare(m));
	}

	// dp[i][j] = min(下面，右边，右下边） + 1 if matrix[i][j] == '1'
	public int maximalSquare(char[][] matrix) {
		if (matrix.length == 0)
			return 0;
		int r = matrix.length, c = matrix[0].length, maxSize = 0;
		int[][] dp = new int[r][c];

		for (int i = r - 1; i >= 0; i--) {
			for (int j = c - 1; j >= 0; j--) {
				if (i == r - 1 || j == c - 1 || matrix[i][j] == '0')
					dp[i][j] = matrix[i][j] - '0';
				else
					dp[i][j] = Math.min(dp[i + 1][j + 1], Math.min(dp[i + 1][j], dp[i][j + 1])) + 1;
				maxSize = Math.max(dp[i][j], maxSize);
			}
		}

		return maxSize * maxSize;
	}
}
