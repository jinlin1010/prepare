package array;

import java.util.ArrayList;
import java.util.List;


public class CombinationSumIII {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CombinationSumIII c = new CombinationSumIII();
		List<List<Integer>> list = c.combinationSum3(3, 15);
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.get(i).size(); j++) {
				System.out.print(list.get(i).get(j));
			}
			System.out.println();
		}
	}

	// backtracking
	public List<List<Integer>> combinationSum3(int k, int n) {
		List<List<Integer>> results = new ArrayList<>();
		if (k == 0 || n == 0) {
			return results;
		}
		List<Integer> result = new ArrayList<>();
		buildCombinationSum3(k, n, 1, results, result);
		return results;
	}

	public void buildCombinationSum3(int k, int remind, int start, List<List<Integer>> results,
			List<Integer> result) {
		if (remind == 0 && result.size() == k) {
			List<Integer> list = new ArrayList<>(result);
			results.add(list);
			return;
		}

		for (int i = start; i <= 9; i++) {
			result.add(i);
			buildCombinationSum3(k, remind - i, i + 1, results, result);
			result.remove(result.size() - 1);
		}
	}

}
