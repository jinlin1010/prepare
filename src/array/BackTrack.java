package array;

import java.util.ArrayList;
import java.util.List;

public class BackTrack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BackTrack b = new BackTrack();
		System.out.println(b.subsets(new int[] { 1, 2, 3 }));

	}

	public List<List<Integer>> subsets(int[] S) {
		List<List<Integer>> res = new ArrayList<>();
		dfs(res, new ArrayList<>(), S, 0);
		return res;
	}

	public void dfs(List<List<Integer>> res, List<Integer> list, int[] arr, int idx) {
		if (idx == arr.length) {
			res.add(new ArrayList<>(list));
			return;
		}
//		for (int i = idx; i < arr.length; i++) {
			list.add(arr[idx]);
			dfs(res, list, arr, idx + 1);
			list.remove(list.size() - 1);
			dfs(res, list, arr, idx + 1);
	//}
	}

}
