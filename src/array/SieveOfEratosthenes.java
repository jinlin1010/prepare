package array;

import java.util.Arrays;

public class SieveOfEratosthenes {
	public static void main (String args[]) {
		SieveOfEratosthenes s = new SieveOfEratosthenes();
		System.out.println(s.countPrime(100));
	}
	
	public int countPrime(int n) {
	   if(n < 2) return 0;
	   Boolean [] prime = new Boolean[n+1];
	   Arrays.fill(prime, true);
	   int res = 0;
	   for(int i=2;  i<n; i++) {
		   if(prime[i] == true) {
			   res++;
			   for(int j=2*i;  j<n; j=i+j) {
				   prime[j] = false;
			   }
		   }
	   }
	   
	   return res;
	}
}
