package array;

public class RotateFunction {
	public static void main (String [] args) {
		RotateFunction r = new RotateFunction();
		int [] arr = { 4, 3, 2, 6};
		System.out.println(r.maxRotateFunction(arr));
	}
	
    public int maxRotateFunction(int[] A) {
    	if (A == null || A.length == 0) return 0;
        int sum = 0, ret = 0, k = 0, l = A.length;
        if (l == 1) return 0;
        for (int n : A) {
            sum += n;
            ret += n * k++;
        }
        int res = ret;
        for (int i = l - 1; i >= 1; i--){
            int curr = ret + sum - l * A[i];
            ret = curr;
            res = Math.max(res, curr);
        }
        return res;
    }
}
