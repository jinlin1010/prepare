package array;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

import util.Print;

public class JumpGameVI {
	static int[] dp = new int[8];

	public static void main(String[] args) {
		int[] arr = { 1, -5, -20, 4, -1, 3, -6, -3 };
		JumpGameVI j = new JumpGameVI();
		Arrays.fill(dp, Integer.MIN_VALUE);
		System.out.println(j.maxResult(arr, 2));
		Print.printArray(dp);
	}

	public int maxResult(int[] nums, int k) {
		if (nums == null || nums.length == 0)
			return 0;
		dp[0] = nums[0];
		for (int i = 0; i < nums.length; i++) {
			dfs(i, nums, k);
		}
		return dp[nums.length - 1];
	}

	public int dfs(int cur, int[] nums, int k) {
		if (dp[cur] != Integer.MIN_VALUE)
			return dp[cur];
		for (int i = 1; i <= k; i++) {
			if (cur - i >= 0) {
				dp[cur] = Math.max(dp[cur], dfs(cur - i, nums, k) + nums[cur]);
			}
		}
		return dp[cur];
	}
	
	//monotonic stack
	public int maxResult1(int[] nums, int k) {
		int [] dp = new int [nums.length];
		Deque <Integer> dq = new ArrayDeque<>();
		for(int i=0; i<nums.length; i++) {
			dp[i] = !dq.isEmpty() ? dp[dq.peek()] : 0 + nums[i];
			while(!dq.isEmpty() && dp[i] > dp [dq.peekLast()]) {
				dq.pollLast();
			}
			dq.offer(i);
			if(i - dq.peek() == k) {
				dq.poll();
			}
		}
		return dp[nums.length - 1];
	}

}
