package array;

public class ContainsDuplicate {
	public static void main(String[] args) {
		ContainsDuplicate c = new ContainsDuplicate();
		int [] nums = {2147483647, -2147483647};
		int k =1;
		int t = 2147483647;
		System.out.println(c.containsNearbyAlmostDuplicate(nums, k, t));
	}

    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        for(int i=0; i<nums.length; i++) {
            int j=i+1;
            while(j<nums.length && j-i <=k) {
                int temp = nums[i] - nums[j];
                System.out.println(nums[i] == Integer.MAX_VALUE);
                System.out.println(nums[j] + " " + Integer.MIN_VALUE);
                System.out.println(temp);
                if(nums[i] == Integer.MAX_VALUE && nums[j] == Integer.MIN_VALUE) {
                    return false;
                }
                if(temp != Integer.MIN_VALUE && Math.abs(temp) <= t) {
                    return true;
                }
                j++;
            }
        }
        return false;
    }
}
