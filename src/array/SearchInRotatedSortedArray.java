package array;

public class SearchInRotatedSortedArray {
	public static void main(String[] args) {
		SearchInRotatedSortedArray s = new SearchInRotatedSortedArray();
		int [] nums =  {3,1};
		System.out.print(s.search(nums, 1));
	}

	public int search(int[] nums, int target) {
		if (nums == null)
			return -1;
		return bst(nums, target, 0, nums.length-1);
	}

	public int bst(int[] nums, int target, int left, int right) {
		int mid = (left + right) / 2;
//		System.out.println(nums[mid] + "--" + nums[left] + "--" + nums[right]);
		if(left > right ) {
			return -1;
		}
		if (nums[mid] == target) {
			return mid;
		} 
		if (target <= nums[mid] && target >= nums[left]) {
			return bst(nums, target, left, mid - 1);
		} else if (target <= nums[mid] && target <= nums[left]) {
		   if (nums[mid] >= nums[left]) {
			   return bst(nums, target, mid + 1, right);
		   } else {
			   return bst(nums, target, left, mid - 1);
		   }
		} else if (target >= nums[mid] && target <= nums[right]) {
			return bst(nums, target, mid + 1, right);
		} else if (target >= nums[mid] && target >= nums[right]) {
			if(nums[mid] < nums[right]) {
				return bst(nums, target, left, mid - 1);
			} else {
				return bst(nums, target, mid + 1, right);
			}
		} 
		return -1;

	}
}
