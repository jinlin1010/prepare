package array;


public class JumpGameV {
	int [] dp = new int [10001];
	public static void main(String args[]) {

		int[] arr = { 6, 4, 14, 6, 8, 13, 9, 7, 10, 6, 12 };

		JumpGameV j = new JumpGameV();
		System.out.println(j.maxJumps(arr, 2));

	}

	public int maxJumps(int[] arr, int d) {
		if(arr == null) return 0;
		for(int i=0; i<arr.length; i++) {
			dfs(i, arr, d);
		}
		return dp[arr.length-1];
	}
	
	public int dfs(int cur, int [] arr, int d) {
		if(dp[cur] !=0) return dp[cur];
		int res = 1;
		for(int i=1; i<=d; i++) {
			if(cur - i < 0) break;
			if(arr[cur] <= arr[cur-i]) break;
			res = Math.max(res, dfs(cur-i, arr, d) + 1);
		}
		for(int i=1; i<=d; i++) {
			if(cur + i >= arr.length) break;
			if(arr[cur] <= arr[cur+1]) break;
			res = Math.max(res, dfs(cur+1, arr, d) + 1);
		}
		dp[cur] = res;
		return res;
	}

}
