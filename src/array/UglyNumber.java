package array;

public class UglyNumber {
	public static void main(String args[]) {
		UglyNumber u = new UglyNumber();
		System.out.println(u.nthUglyNumber(12));
		int[] arr = { 2, 7, 13, 19 };
		System.out.println(u.nthSuperUglyNumber(15, arr));
		System.out.println(u.nthUglyNumber(4, 2, 3, 4));

	}

	public int nthUglyNumber(int n) {
		int[] nums = new int[1690];
		int i2 = 0, i3 = 0, i5 = 0, i7 = 0, ugly;
		nums[0] = 1;
		for (int i = 1; i < 1690; i++) {
			ugly = Math.min(Math.min(Math.min(2 * nums[i2], 7 * nums[i3]), 13 * nums[i5]), 19 * nums[i7]);
			// System.out.println("ugly: " + ugly);
			nums[i] = ugly;
			if (ugly == 2 * nums[i2])
				i2++; // check ugly 是不是 existing nums[i2] 倍数
			if (ugly == 7 * nums[i3])
				i3++;
			if (ugly == 13 * nums[i5])
				i5++;
			if (ugly == 19 * nums[i7])
				i7++;
		}

		for (int i = 0; i < n; i++) {
			System.out.println(nums[i]);
		}
		return nums[n - 1];
	}

	// 2, 7, 13, 19 -> primes
	// i2, i3, i5, i7 -> idx
	public int nthSuperUglyNumber(int n, int[] primes) {
		if (n == 1)
			return 1;
		if (primes.length == 0)
			return 0;
		int[] dp = new int[n];
		int[] idx = new int[primes.length];
		dp[0] = 1;
		int ugly;
		for (int i = 1; i < n; i++) {
			ugly = findMin(idx, primes, dp);
			dp[i] = ugly;
			for (int j = 0; j < primes.length; j++) {
				if (ugly == dp[idx[j]] * primes[j]) {
					idx[j]++;
				}
			}
		}
		for (int i = 0; i < n; i++) {
			System.out.println(dp[i]);
		}
		return dp[n - 1];
	}

	public int findMin(int[] idx, int[] primes, int[] dp) {
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < idx.length; i++) {
			min = Math.min(min, dp[idx[i]] * primes[i]);
		}
		return min;
	}

	public int nthUglyNumber(int n, int a, int b, int c) {
		if (n == 0)
			return 0;
		int[] dp = new int[n];
		int ugly;
		int a_idx = 1, b_idx = 1, c_idx = 1;

		for (int i = 0; i < n; i++) {
			ugly = Math.min(Math.min(a_idx * a, b_idx * b), c_idx * c);
			dp[i] = ugly;
			if (ugly == a_idx * a)
				a_idx++;
			if (ugly == b_idx * b)
				b_idx++;
			if (ugly == c_idx * c)
				c_idx++;
		}
		for (int i = 0; i < n; i++) {
			System.out.println(dp[i]);
		}
		return dp[n - 1];
	}
}
