package array;

public class LongestSubstringwithAtLeastKRepeatingCharacters {
  public static void main (String args []) {
	  LongestSubstringwithAtLeastKRepeatingCharacters l = new LongestSubstringwithAtLeastKRepeatingCharacters();
	  System.out.println(l.longestSubstring("aaabb", 3));
  }
  
  public int longestSubstring(String s, int k) {
      if(s == null || s.length() == 0) return 0;
      int [] hash = new int [26];
      for(int i=0; i<s.length(); i++) {
          hash[s.charAt(i) - 'a'] ++;
      }
      boolean fullString = true;
      for(int i=0; i<s.length(); i++) {
          if(hash[s.charAt(i) - 'a'] > 0 && hash[s.charAt(i) - 'a'] < k){
              fullString = false;
          }
      }
      // all character Occurrences are larger than k
      if(fullString == true)  return s.length();
      
      // two pointer cut the occurrences that smaller than k
      // recursive take substring to find the max length
      int begin =0; int end=0; int res =0;
      while(end < s.length()) {
          if(hash[s.charAt(end) - 'a'] < k) {
              String newString = s.substring(begin, end);
              res = Math.max(res, longestSubstring(newString, k));
              begin = end+1;
          }
          end++;
      }
      res = Math.max(res, longestSubstring(s.substring(begin), k));
      return res;
  }
}
