package array;

public class SearchMatrix {
	public static void main(String[] args) {
		int[][] m = { { 1, 4, 7, 11, 15 }, { 2, 5, 8, 12, 19 }, { 3, 6, 9, 16, 22 }, { 10, 13, 14, 17, 24 },
				{ 18, 21, 23, 26, 30 } };
		
		int [][] m1 = {{1,4}, {2,5}};
		//System.out.println(m1[0][1]);
		SearchMatrix s = new SearchMatrix();
		System.out.println(s.searchMatrix(m, 5));

	}

    public boolean searchMatrix(int[][] matrix, int target) {
        if(matrix.length == 0) return false;
        if(matrix[0].length == 0) return false;
        int row = matrix.length;
        int column = matrix[0].length;
        int rowIdx = 0;
        for(int i=0; i<column; i++) {
            if(matrix[0][i] == target) {
                return true;
            } else if(matrix[0][i] < target) {
                rowIdx = i;
            } 
        } 
        
        for(int i=0; i<row; i++) {
            if(matrix[i][rowIdx] == target) {
                return true;
            }
        }
        
        return false;
    }
}
