package array;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JumpGameIV {
	public static void main(String[] args) {
		JumpGameIV j = new JumpGameIV();
		int[] arr = { 7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7,
				7, };
		System.out.println(j.minJumps(arr));
	}

	public int minJumps(int[] arr) {
		if (arr == null)
			return 0;
		int[] dp = new int[arr.length];
		Arrays.fill(dp, Integer.MAX_VALUE);
		dp[0] = 0;
		Deque<Integer> dq = new ArrayDeque<>();
		Set<Integer> s = new HashSet<>();
		Map<Integer, List<Integer>> m = new HashMap<>();
		for (int i = 0; i < arr.length; i++) {
			m.putIfAbsent(arr[i], new ArrayList<>());
			m.get(arr[i]).add(i);
		}
		dq.offer(0);
		while (!dq.isEmpty() && dq.peek() != dp.length - 1) {
			int i = dq.poll();
			if (i > 0) {
				dp[i - 1] = Math.min(dp[i] + 1, dp[i - 1]);
				if (!s.contains(i - 1)) {
					dq.offer(i - 1);
					s.add(i - 1);
				}
			}
			if (i < arr.length - 1) {
				dp[i + 1] = Math.min(dp[i] + 1, dp[i + 1]);
				if (!s.contains(i + 1)) {
					dq.offer(i + 1);
					s.add(i + 1);
				}

			}
			if (m.containsKey(arr[i])) {
				for (int j = 0; j < m.get(arr[i]).size(); j++) {
					int k = m.get(arr[i]).get(j);
					if (k != i) {
						dp[k] = Math.min(dp[k], dp[i] + 1);
						if (!s.contains(k)) {
							dq.offer(k);
							s.add(k);
						}
					}
				}
				m.remove(arr[i]);
			}

		}
		return dp[dp.length - 1];
	}
}
