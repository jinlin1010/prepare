package array;

public class Fibonacci {

	public static void main(String[] args) {
		Fibonacci f = new Fibonacci();
      System.out.println(f.findFibonacci2(7));
	}
	
	public int findFibonacci(int n) {
		int a = 1;
		int b = 1;
		int c = 0;
		if (n<2) return 1;	
		for(int i=n; i>2; i--) {
			c = a + b;
			a = b;
			b = c;
		}
		return c;
	}
	
	public int findFibonacci2(int n) {
		if(n<2) return n;
		return findFibonacci2(n-1) + findFibonacci2(n-2);
	}
	
	public int findFibonacci3(int n) {
		 Integer [] arr = new Integer[n+1];
	     return fib(n, arr);
	}
	
	public static int fib(int n, Integer [] arr) {
        if(arr[n] != null) {
            return arr[n];
        }
        int result;
        if (n < 2) {
            result = n;
        } else {
            result = fib(n-1, arr) + fib(n-2, arr);
        }
        arr[n] = result;
        return result;
    }
	
    public int climbStairs(int n) {
        Integer [] arr = new Integer [n+1];
        return climbStairs(n, arr);
    }
    
    public int climbStairs(int n, Integer [] arr) {
        if( arr[n] != null ) {
          return arr[n];
        }
        int result;
        if(n == 0 || n == 1) {
           result = 1; 
        } else {
           result = climbStairs(n-1, arr) + climbStairs(n-2, arr);
        }
        
        arr[n] = result;
        return result;
    }
}
