package array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSumII {

	public static void main(String args[]) {

	}

	public List<List<Integer>> combinationSum2(int target, int[] num) {

		Arrays.sort(num);
		List<List<Integer>> results = new ArrayList<>();
		combinationSum2(target, num, 0, new ArrayList<Integer>(), results);
		return results;
	}

	public void combinationSum2(int target, int[] num, int start, List<Integer> result, List<List<Integer>> results) {

		if (target == 0) {
			List<Integer> list = new ArrayList<>(result); // list structure will update, so maintain a new one
			results.add(list);
			return;
		}

		for (int i = start; i < num.length; i++) {
			if (num[i] > target)
				continue;
			result.add(num[i]);
			combinationSum2(target - num[i], num, i + 1, result, results);
			result.remove(result.size() - 1);
			while (i < num.length - 1 && num[i] == num[i + 1]) {
				i++;
			}
		}
	}
}
