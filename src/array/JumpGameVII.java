package array;

public class JumpGameVII {
	public static void main(String[] args) {
		JumpGameVII j = new JumpGameVII();
		System.out.println(j.canReach("01101110", 2, 3));
	}

	public boolean canReach(String s, int minJump, int maxJump) {
		boolean[] dp = new boolean[s.length()];
		dp[0] = true;
		int i = 0;
		for (int j = 1; j < s.length() && j > i; j++) {
			if (s.charAt(j) == '0') {
				if (i + minJump <= j && j <= Math.min(i + maxJump, s.length() - 1)) {
					dp[j] = true;
				} else if (j > Math.min(i + maxJump, s.length() - 1)) {
					i++;
					j--;
					while (i < s.length() && dp[i] == false) {
						i++;
					}
				}

			}
		}

		return dp[s.length() - 1];
	}
}
