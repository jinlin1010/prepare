package array;

import java.util.Random;

public class InverseNumberPair {

	private int count = 0; // 逆数对的数量

	public static int[] geneateArrays(int n) {
		int arr[] = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = new Random().nextInt(n) + 1;
		}
		return arr;
	}

	public void mergesort(int[] arr, int n) {
		__mergesort(arr, 0, n - 1);
	}

	public void __mergesort(int[] arr, int l, int r) {

		if (l >= r)
			return;

		int mid = (l + r) / 2;
		__mergesort(arr, l, mid);
		__mergesort(arr, mid + 1, r);
		if (arr[mid] > arr[mid + 1]) // 若arr[mid]<arr[mid+1]，则数组已经有序，if判断下减少了归并步骤
			__merge(arr, l, mid, r);
	}

	// 将arr[l,mid]和arr[mid+1,r]两个部分进行归并
	public void __merge(int[] arr, int l, int mid, int r) {
		int aux[] = new int[r - l + 1];
		for (int i = l; i <= r; i++) {
			aux[i - l] = arr[i]; // i-l:减去偏移量
		}
		int i = l, j = mid + 1; // 分别指向两个数组的开头
		for (int k = l; k <= r; k++) {

			if (i > mid) { // 若aux[]左半边的元素都访问完了，则将aux右半边剩余元素赋值回arr[]
				arr[k] = aux[j - l];
				j++;
			} else if (j > r) { // 若aux[]右半边的元素都访问完了，则将aux左半边剩余元素赋值回arr[]
				arr[k] = aux[i - l];
				i++;
			} else if (aux[j - l] < aux[i - l]) { // 在aux[]中比较后，将正确结果赋值回arr[]
				count = count + mid - i + 1;
				arr[k] = aux[j - l];
				j++;
			} else { // aux[i-l]<=aux[j-l]
				arr[k] = aux[i - l];
				i++;
			}

		}
	}

	public int count() {
		return count;
	}

	public static void main(String[] args) {
		int arr[] = { 3, 2, 1, 4, 6, 7, 9, 5, 2 };
		for (int i = 0; i < arr.length; i++) {
			System.out.print(" " + arr[i]);
		}
		InverseNumberPair in = new InverseNumberPair();
		in.mergesort(arr, arr.length);
		System.out.println();
		System.out.println("逆数对的数量: " + in.count());

	}

}