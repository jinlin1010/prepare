package array;

// fewest number of coins that you need to make up that amount.
public class CoinChange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CoinChange c = new CoinChange();
		int[] arr = { 2,3, 5 };
		System.out.println(c.coinChange(arr, 10));
		//System.out.println(c.numSquares(12));
	}

	// dp[i] = Min(dp[i], dp[i-coins[j]]+1) if i-coins[j] > 0
	public int coinChange(int[] coins, int amount) {
		int[] dp = new int[amount+1];
		for (int i = 1; i < dp.length; i++) {
			dp[i] = 999999;
			for (int j = 0; j < coins.length; j++) {
				if (i > coins[j])
					continue;
				dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);

			}
		}

		return dp[dp.length - 1] == 999999 ? -1 : dp[dp.length - 1];
	}

	public int numSquares(int n) {
		int len = (int) Math.sqrt(n);
		int[] ps = new int[len];
		for (int i = 1; i < ps.length; i++) {
			ps[i] = i * i;
		}
		int[] dp = new int[n + 1];

		for (int i = 1; i < dp.length; i++) {
			dp[i] = Integer.MAX_VALUE;
			for (int j = 0; j < ps.length; j++) {
				if (i - ps[j] < 0) {
					continue;

				} else {
					dp[i] = Math.min(dp[i], dp[i - ps[j]] + 1);
				}
			}
		}

		return dp[dp.length - 1];
	}
}
