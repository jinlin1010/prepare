package array;

import java.util.Arrays;

public class JumpGameIII {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JumpGameIII j = new JumpGameIII();
		int[] arr = { 4, 2, 3, 1, 3, 1, 2 };
		int start = 5;
		Arrays.fill(arr, Integer.MAX_VALUE);
		System.out.println(j.canReach(arr, start));
	}

	public boolean canReach(int[] arr, int start) {
		boolean[] visited = new boolean[arr.length];
		return canReach(arr, start, visited);
	}

	public boolean canReach(int[] arr, int start, boolean[] visited) {
		if (arr[start] == 0) {
			return true;
		}
		boolean left = false;
		boolean right = false;
		int nextLeft = start - arr[start];
		int nextRight = start + arr[start];
		if (nextLeft >= 0 && !visited[nextLeft]) {
			visited[nextLeft] = true;
			left = canReach(arr, nextLeft, visited);
		}
		if (nextRight < arr.length && !visited[nextRight]) {
			visited[nextRight] = true;
			right = canReach(arr, nextRight, visited);
		}
		return left || right;
	}

}
