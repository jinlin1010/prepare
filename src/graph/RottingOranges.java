package graph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class RottingOranges {
	public static void main (String args[]) {
		RottingOranges o = new RottingOranges();
		 // int[][] grid = { { 2, 1, 1 }, { 1, 1, 1 }, {0, 1, 2} };
		int[][] grid = { {1}, {2} } ;
		System.out.println(o.orangesRotting(grid));
	}

	public int orangesRotting(int[][] grid) {
        Queue<Node> s = new LinkedList<>();
		if(grid == null || grid.length == 0) return -1;
		for(int i=0; i<grid.length; i++) {
			for(int j=0; j<grid[0].length; j++) {
				if(grid[i][j] == 2) {
					s.offer(new Node(i, j, 0));
				}
			}
		}
		int res = 0;
		while(!s.isEmpty()) {
			Node n = s.remove();
			int i = n.x, j=n.y, cnt = n.cnt;
			if(i - 1 >= 0 && grid[i-1][j] == 1) {
				grid[i-1][j] = 2;
				s.offer(new Node(i-1, j, cnt+1));
			}
			if(i + 1 < grid.length && grid[i+1][j] == 1) {
				grid[i+1][j] = 2;
				s.offer(new Node(i+1, j, cnt+1));
			}
			if(j - 1 >= 0 && grid[i][j-1] == 1) {
				grid[i][j-1] = 2;
				s.offer(new Node(i, j-1, cnt+1));
			}
			if(j + 1 < grid[0].length && grid[i][j+1] == 1) {
				grid[i][j+1] = 2;
				s.offer(new Node(i, j+1, cnt+1));
			}
			res = Math.max(res, cnt);
		}
		for(int i=0; i<grid.length; i++) {
			for(int j=0; j<grid[0].length; j++) {
				if(grid[i][j] == 1) {
					res = -1;
				}
			}
		}
		return res;
	}
	
	class Node{
		int x;
		int y;
		int cnt;
		Node(int x, int y, int cnt){
			this.x = x;
			this.y = y;
			this.cnt = cnt;
		}
	}
}
