package graph;

public class MinimumFallingPathSum {
	int result = Integer.MAX_VALUE;

	public static void main(String[] args) {
		int[][] A = new int[][] { { -80, -13, 22 }, { 83, 94, -5}, { 73, -48, 61} };
		MinimumFallingPathSum m = new MinimumFallingPathSum();
		System.out.println(m.minFallingPathSum1(A));
	}

	public int minFallingPathSum(int[][] A) {
		for (int i = 0; i < A.length; i++) {
			dfs(A, 0, i, A[0][i]);
		}
		return result;
	}

	public void dfs(int[][] A, int i, int j, int min) {
		if (i == A.length - 1) {
			result = Math.min(min, result);
			return;
		}

		// 左下
		if (j > 0) {
			// 左下
			dfs(A, i + 1, j - 1, min + A[i + 1][j - 1]);
		}
		if (j < A.length - 1) {
			// 右下
			dfs(A, i + 1, j + 1, min + A[i + 1][j + 1]);
		}
		dfs(A, i + 1, j, min + A[i + 1][j]);

	}

	public int minFallingPathSum1(int[][] A) {
		if (A == null || A.length == 0)
			return 0;
		if (A.length == 1 && A[0].length == 1)
			return A[0][0];
		int[][] dp = new int[A.length][A.length];
		int result = Integer.MAX_VALUE;
		for(int i=0; i<A.length; i++) {
			dp[0][i] = A[0][i];
		}
		for (int i = 1; i < A.length; i++) {
			for (int j = 0; j < A.length; j++) {
				dp[i][j] = A[i][j] + dp[i - 1][j];
				if (j > 0) {
					dp[i][j] = Math.min(dp[i][j], A[i][j] + dp[i - 1][j - 1]);
				}
				if (j < A.length - 1) {
					dp[i][j] = Math.min(dp[i][j], A[i][j] + dp[i - 1][j + 1]);
				}
			}
		}
		for (int i = 0; i < A.length; i++) {
			result = Math.min(result, dp[A.length - 1][i]);
		}
		return result;
	}

}
