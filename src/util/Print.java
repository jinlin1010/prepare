package util;

import java.util.List;

public class Print<T> {
	T obj;

	public Print(T obj) {
		this.obj = obj;
	}

	public void print() {
		System.out.println(obj);
	}

	public static void printIntegerList(List<Integer> input) {
		for (Integer i : input) {
			System.out.print(i);
		}
	}

	public static void printStringList(List<String> input) {
		for (String i : input) {
			System.out.println(i);
		}
	}

	public static void printDoubleArray(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void printDoubleArray(boolean[][] arr) {
		System.out.println("##########################");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}

	public static void printArray(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
			System.out.println(arr[i]);
		}
	}

	public static void printDoubleList(List<List<String>> list) {
		System.out.println("##########################");
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.get(i).size(); j++) {
				System.out.print(list.get(i).get(j) + " ");
			}
			System.out.println();

		}
	}
	
	public static void printDoubleIntegerList(List<List<Integer>> list) {
		System.out.println("##########################");
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < list.get(i).size(); j++) {
				System.out.print(list.get(i).get(j) + " ");
			}
			System.out.println();

		}
	}
}
