package string;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class KSimilarity {
	public static void main (String [] args) {
		KSimilarity k = new KSimilarity();
		System.out.println(k.kSimilarity("abccaacceecdeea", "bcaacceeccdeaae"));
	}
    public int kSimilarity(String A, String B) {
        HashSet<String> seen = new HashSet<>(); 
        seen.add(A);
        Queue<String> q = new LinkedList<>(); 
        q.offer(A);
        int step = 0;
        while (!q.isEmpty()) {
            for (int size = q.size(); size > 0; size--) {
                String str = q.poll();
                if (str.equals(B)) return step;
                int i = 0;
                while (i < str.length() && str.charAt(i) == B.charAt(i))
                    i++; // Find index of char that is different between `str` and `B`  skip 前面一样的
                for (int j = i + 1; j < str.length(); j++) {
                    if (str.charAt(j) == B.charAt(j)) continue; // Skip 后面相同的j index -  Skip index `j` of char that `str` and `B` are already the same
                    if (str.charAt(j) != B.charAt(i)) continue; // Skip 跟i不同的j index - Skip util found index `j` of `str` that str[j] = B[i]
                    String newStr = swap(str, i, j); // Swap to make str[i] the same with B[i]
                    if (!seen.contains(newStr)) {
                        q.offer(newStr);
                        seen.add(newStr);
                    }
                }
            }
            step++;
        }
        return step;
    }
    private String swap(String str, int i, int j) {
        char[] chars = str.toCharArray();
        char temp = chars[i];
        chars[i] = chars[j];
        chars[j] = temp;
        return new String(chars);
    }
}
