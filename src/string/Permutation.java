package string;

import java.util.ArrayList;
import java.util.List;

import util.Print;

public class Permutation {
	public static void main(String args[]) {
		String input = "aeiou";
		Permutation p = new Permutation();
		Print.printStringList(p.getAllPermutation(input));
	}

	public List<String> getAllPermutation(String str) {
		List<String> res = new ArrayList<String>();
		permute(res, str, "");
		return res;
	}

	private void permute(List<String> res, String str, String ans) {
	  if (str.length() == 0) {  // with rest if empty string
		  res.add(ans);
		  return;
	  }
	  
	  for(int i=0; i<str.length(); i++) {
		  char cur = str.charAt(i);
		  String rest = str.substring(0, i) + str.substring(i+1); // rest of string after remove current index i;
		  permute(res, rest, ans + cur);  // 1. replace rest with str,  2. adding ans with current. 
	  }
	}
}
