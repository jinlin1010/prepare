package string;

/*
 Example:

Input:
"abccccdd"

Output:
7

Explanation:
One longest palindrome that can be built is "dccaccd", whose length is 7.

 */
public class LongestPalindromeLength {
	public static void main(String[] args) {
		LongestPalindromeLength l = new LongestPalindromeLength();
		System.out.println(l.longestPalindrome("abccccdd"));
	}

	// charArray to store, 有2个以上只能生成回文， length = same char >2 + odd
	public int longestPalindrome(String s) {
		if (s.length() == 0 || s == null)
			return 0;
		int[] charArr = new int[52];
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				charArr[s.charAt(i) - 'A' + 26]++;
			} else {
				charArr[s.charAt(i) - 'a']++;
			}
		}
		int hasOdd = 0;
		for (int i = 0; i < charArr.length; i++) {
			if (charArr[i] % 2 == 1) {
				hasOdd++;
			}
		}
		if (hasOdd <= 1) {
			return s.length();
		} else {
			return s.length() - (hasOdd - 1);
		}
	}
}
