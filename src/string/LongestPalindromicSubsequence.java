package string;

import util.Print;

public class LongestPalindromicSubsequence {

	public static void main(String[] args) {
		LongestPalindromicSubsequence l = new LongestPalindromicSubsequence();
		System.out.print(l.longestPalindromeSubseq("bbbab"));

	}
    // From right bottom to right top，if(s(i)==s(j)) dp[i][j] = lowerLeft+2，else max(left, bottom)
	public int longestPalindromeSubseq(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		int n = s.length();
		int[][] dp = new int[n][n];
		for (int i = n - 1; i >= 0; i--) {
			dp[i][i] = 1;
			for (int j = i + 1; j < n; j++) {
				System.out.println(i + " " + j);
				if (s.charAt(i) == s.charAt(j)) {
					dp[i][j] = dp[i + 1][j - 1] + 2;
				} else {
					dp[i][j] = Math.max(dp[i][j - 1], dp[i + 1][j]);
				}
				System.out.println(dp[i][j]);
				Print.printDoubleArray(dp);
			}
		}
		return dp[0][n - 1];
	}

}
