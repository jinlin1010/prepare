package string;

/*
 * 
Given s1, s2, s3, find whether s3 is formed by the interleaving of s1 and s2.

Example 1:

Input: s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
Output: true
 * 
 */
public class InterleavingString {

	public static void main(String[] args) {
		String inputStr1 = "abc";
		String inputStr2 = "cd";
		String inputStr3 = "acbcd";
		// output true;

		InterleavingString i = new InterleavingString();
		System.out.println(i.isInterleave(inputStr1, inputStr2, inputStr3));

	}

	// dp,  matched[m,n] = matched[m-1, n] + matched[m, n-1]
	public boolean isInterleave(String s1, String s2, String s3) {
        if(s1.length() + s2.length() != s3.length()) return false;
        boolean [][] matched = new boolean[s1.length()+1][s2.length()+1];
        matched[0][0] = true;
        // fill true with same prefix on row
        for(int i=0; i<s1.length(); i++){
            if(s1.charAt(i) == s3.charAt(i)) {
                matched[i+1][0] = true;
            } else {
                break;
            }
        }
        // fill true with same prefix on column
        for(int i=0; i<s2.length(); i++){
            if(s2.charAt(i) == s3.charAt(i)){
                matched[0][i+1] = true;
            } else {
                break;
            }
        }
        for(int i=1; i<=s1.length(); i++){
            for(int j=1; j<=s2.length(); j++){
                int k = i + j;
                if(s1.charAt(i-1) == s3.charAt(k-1)){
                    matched[i][j] |= matched[i-1][j];
                }
                if(s2.charAt(j-1) == s3.charAt(k-1)){
                    matched[i][j] |= matched[i][j-1];
                }
            }
        }
        return matched[s1.length()][s2.length()];
    }
}
