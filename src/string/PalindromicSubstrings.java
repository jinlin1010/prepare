package string;

public class PalindromicSubstrings {
    public static void main (String args[]) {
    	PalindromicSubstrings p = new PalindromicSubstrings();
    	//System.out.println(p.countSubstrings("aaa"));
    	System.out.println(p.countSubstrings("abc"));
    	//System.out.println(p.countSubstrings("aba"));

    }
    public int countSubstrings(String s) {
    	if(s == null || s.length() == 0) return 0;
    	int result =0;
    	for(int i=0; i<s.length(); i++) {
    		int left, right;
    		left = i;
    		right = i;
    		
    		while(left > 0 && right <s.length() && s.charAt(left) == s.charAt(right)) {
    			left --;
    			right ++;
    			result ++;
    		}
    	}
        return result;
    }
}
