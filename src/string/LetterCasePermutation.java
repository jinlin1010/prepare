package string;

/**
 Given a string S, we can transform every letter individually to be lowercase or uppercase to create another string.  Return a list of all possible strings we could create.

Examples:
Input: S = "a1b2"
Output: ["a1b2", "a1B2", "A1b2", "A1B2"]

Input: S = "3z4"
Output: ["3z4", "3Z4"]

Input: S = "12345"
Output: ["12345"]
 */
import java.util.ArrayList;
import java.util.List;

import util.Print;

public class LetterCasePermutation {

	public static void main(String[] args) {
		String input = "12345";
		LetterCasePermutation l = new LetterCasePermutation();
		Print.printStringList(l.letterCasePermutation(input));

	}
    
	// appending item to list, then operate it, making fix size outside of loop
	public List<String> letterCasePermutation(String S) {
		List<String> res = new ArrayList<>();
		res.add(S);
		for (int i = 0; i < S.length(); i++) {
			if (Character.isLetter(S.charAt(i))) {
				int size = res.size();
				for (int j = 0; j < size; j++) {
					char c = S.charAt(i);
					if (Character.isLowerCase(c)) {
						c = Character.toUpperCase(c);
					} else {
						c = Character.toLowerCase(S.charAt(i));
					}
					String str = res.get(j).substring(0, i) + c + res.get(j).substring(i + 1);
					res.add(str);

				}
			}
		}
		return res;
	}

}
