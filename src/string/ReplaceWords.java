package string;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ReplaceWords {
	public static void main(String args[]) {
		List<String> dict = new ArrayList<>();
		dict.add("cat");
		dict.add("bat");
		dict.add("rat");
		String sentence = "the cattle was rattled by the battery";
		ReplaceWords rp = new ReplaceWords();
		System.out.println(rp.replaceWords(dict, sentence));
	}

	public String replaceWords(List<String> dict, String sentence) {
		Set<String> set = new HashSet<>(dict);
		String[] words = sentence.split(" ");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < words.length; i++) {
			for (int j = 1; j <= words[i].length(); j++) {
				String root = words[i].substring(0, j);
				if (set.contains(root)) {
					words[i] = root;
				}
			}
			sb.append(words[i] + " ");
		}
		return sb.substring(0, sb.length() - 1);
	}
}