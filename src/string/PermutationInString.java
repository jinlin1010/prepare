package string;

import java.util.Arrays;

import util.Print;


// sliding window, compare two arrays
public class PermutationInString {
	public static void main(String[] args) {
		PermutationInString p = new PermutationInString();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Print print = new Print(p.checkInclusion("bc", "abcd"));
		print.print();
	}
	
	public boolean checkInclusion(String s1, String s2) {
	    if(s1.length() > s2.length()) return false;
		int [] arr1 = new int [26];
		int [] arr2 = new int [26];
		for(int i=0; i<s1.length(); i++) {
			arr1[s1.charAt(i) - 'a'] ++;
			arr2[s2.charAt(i) - 'a'] ++;
		}
		if(Arrays.equals(arr1, arr2)) return true;
		for(int i=s1.length(); i<s2.length(); i++) {
			arr2[s2.charAt(i-s1.length()) - 'a'] --;
			arr2[s2.charAt(i) - 'a']++;
			if(Arrays.equals(arr1, arr2)) {
				return true;
			}
		}
		return false;
	}
	
}
