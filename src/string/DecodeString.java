package string;

public class DecodeString {
	public static void main(String[] args) {
		DecodeString d = new DecodeString();
		System.out.println(d.decodeString("3[a]2[bc]"));
		System.out.println(d.decodeString("3[a2[c]]"));
		System.out.println(d.decodeString("2[abc]3[cd]ef"));
		// System.out.println(d.extendString("[abc]", 3));
		System.out.println(d.decodeString("100[leetcode]"));

	}

	public String decodeString(String s) {
		if (!s.contains("[")) {
			return s;
		}
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ']') {
				int j = i;
				while (j > 0) {
					if (s.charAt(j) == '[') {
						int k = j - 1;
						int nums = 0;
						int m = 0;
						// find out the digit
						while (k >= 0 && Character.isDigit(s.charAt(k))) {
							int digit = (int) s.charAt(k) - (int) '0';
							m = j - k - 1;
							nums += digit * Math.pow(10, m);
							k--;
						}

						// pass [***] -> extend string
						String replaceStr = extendString(s.substring(j, i + 1), nums);

						// replace aaa with 3[a]
						s = s.substring(0, j - m - 1) + replaceStr + s.substring(i + 1, s.length());
						return decodeString(s);
					}
					j--;
				}
				break;
			}
		}

		return s;
	}

	public String extendString(String s, int len) {
		StringBuilder sb = new StringBuilder();
		String strContent = s.substring(1, s.length() - 1);
		for (int i = 0; i < len; i++) {
			sb.append(strContent);
		}
		return sb.toString();
	}
}
