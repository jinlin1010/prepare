package string;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class ShortestSuperstring {
	class State {
		String word;
		int mask;

		public State(String word, int mask) {
			this.word = word;
			this.mask = mask;
		}

	}

	public String shortestSuperstring(String[] A) {
		PriorityQueue<State> pq = new PriorityQueue<>((a, b) -> a.word.length() - b.word.length());
		Set<Integer> set = new HashSet<>();

		int endMask = 0;
		for (int i = 0; i < A.length; i++) {
			int temp = 1 << i;
			endMask |= temp;
		}

		pq.add(new State("", 0));

		while (!pq.isEmpty()) {
			State cur = pq.poll();
			if (cur.mask == endMask) {
				return cur.word;
			}

			if (set.contains(cur.mask))
				continue;

			set.add(cur.mask);

			for (int i = 0; i < A.length; i++) {
	            System.out.println("cur.mask: " + cur.mask + " 1<<i: " + (1<<i) + " " + ((cur.mask & (1 << i)) == 0));

				if ((cur.mask & (1 << i)) == 0) {

					String nextStr1 = merge(cur.word, A[i]);
					String nextStr2 = merge(A[i], cur.word);
					int nextMask = cur.mask | (1 << i);
					pq.add(new State(nextStr1, nextMask));
					pq.add(new State(nextStr2, nextMask));
				}
			}
		}

		return null;
	}

	private String merge1(String head, String tail) {
		int i = 0, j = 0;
		while (i < head.length()) {
			if (j < tail.length() && head.charAt(i) == tail.charAt(j)) {
				i++;
				j++;
			} else if (j >= tail.length() && j != 0) {
				j = 0;
			} else if (j != 0) {
				j = 0;
			} else {
				i++;
			}
		}

		return head + tail.substring(j);
	}
	
    public String merge(String a, String b) {
    	System.out.println(a + " : " + b);
        int repeatCount = 0;
        int leftIndex = 0;
        int rightIndex = 0;
        while(leftIndex < a.length()) {
            if(rightIndex < b.length() && a.charAt(leftIndex) == b.charAt(rightIndex)){
                repeatCount ++;
                rightIndex ++;
            } else {
                repeatCount = 0;
                rightIndex = 0;
            }
            leftIndex ++;
        }
        return a + b.substring(repeatCount);
    }
	
	public static void main (String [] args) {
		ShortestSuperstring s = new ShortestSuperstring();
		String [] arr = {"catg","ctaagt","gcta","ttca","atgcatc"};
		 System.out.println(s.shortestSuperstring(arr));
		//System.out.println(s.merge1("catg", "gcta"));

	}
}
