package string;

public class SuperPalindromes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SuperPalindromes s  = new SuperPalindromes();
		System.out.print(s.superpalindromesInRange("4", "1000"));
		//System.out.print(s.isPalindromes(17161l));
	}

//	public int superpalindromesInRange(String L, String R) {
//		int count = 0;
//		Long leftLong = Long.valueOf(L);
//		Long rightLong = Long.valueOf(R);
//        Double leftSqrt = Math.sqrt(leftLong);
//        Double rightSqrt = Math.sqrt(rightLong);
//        Long leftSqrtLong = leftSqrt.longValue();
//        Long rightSqrtLong = rightSqrt.longValue();
//        for(long i=leftSqrtLong; i<=rightSqrtLong; i++) {
//        	
//        	  if(isPalindrom(i)) {
//        		  Double d = Math.pow(i, 2);
//        		  System.out.println("isPalindromes d " + i);
//        		  if(isPalindrom(d.longValue())) {
//        			  // System.out.println("isPalindromes s " + d);
//        			  count ++;
//        		  }
//        	  }
//        }
//		return count;
//	}
	
	
	public int superpalindromesInRange(String L, String R) {
		int low = (int)Math.ceil(Math.sqrt(Long.parseLong(L)));
		int high = (int)Math.floor(Math.sqrt(Long.parseLong(R)));

		int ret = (3 >= low && 3 <= high) ? 1 : 0;
		ret += dfs(low, high, "");
		ret += dfs(low, high, "0");
		ret += dfs(low, high, "1");
		ret += dfs(low, high, "2");

		return ret;
	}

	private int dfs(int low, int high, String s) {
		if (s.length() > Integer.toString(high).length()) return 0;

		int count = 0;

		if (!s.isEmpty() && s.charAt(0) != '0') {
			long num = Long.parseLong(s);
			if (num > high) return 0;
			if (num >= low && isPalindrom(num * num)) count++;
		}

		for (char c = '0'; c <= '2'; c++) {
			count += dfs(low, high, c + s + c);
		}

		return count;
	}

	private boolean isPalindrom(long num) {
		String s = Long.toString(num);
		return s.equals(new StringBuilder(s).reverse().toString());
	}

}
