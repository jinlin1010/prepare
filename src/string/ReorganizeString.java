package string;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class ReorganizeString {
	public static void main(String[] args) {
		ReorganizeString r = new ReorganizeString();
		System.out.println(r.reorganizeString1("aab"));
	}

	public String reorganizeString(String S) {
		Map<Character, Integer> hash = new HashMap<>();
		for (char c : S.toCharArray()) {
			hash.put(c, hash.getOrDefault(c, 0) + 1);
		}
		// build maxHeap
		PriorityQueue<Character> maxHeap = new PriorityQueue<>((a, b) -> hash.get(b) - hash.get(a));
		maxHeap.addAll(hash.keySet());
		StringBuilder result = new StringBuilder();
		while (maxHeap.size() > 1) {
			char current = maxHeap.remove();
			// if(maxHeap.size() > 0)
			char next = maxHeap.remove();
			result.append(current);
			// if(maxHeap.size() > 0)
			result.append(next);
			hash.replace(current, hash.get(current) - 1);
			hash.replace(next, hash.get(next) - 1);
			if (hash.get(current) > 0) {
				maxHeap.add(current);
			}
			if (hash.get(next) > 0) {
				maxHeap.add(next);
			}
		}
		if (!maxHeap.isEmpty()) {
			char last = maxHeap.remove();
			if (hash.get(last) > 1) {
				return "";
			}
			result.append(last);
		}
		return result.toString();
	}
	
    public String reorganizeString1(String S) {
        if(S == null || S.length() == 0) return S;
        Map <Character, Integer> map = new HashMap<>();
        for(int i=0; i<S.length(); i++) {
            map.put(S.charAt(i), map.getOrDefault(S.charAt(i), 0) + 1);
        }
        PriorityQueue<Character> heap = new PriorityQueue<>((a,b) -> map.get(b) - map.get(a));
        heap.addAll(map.keySet());
        StringBuilder sb = new StringBuilder();
        while(heap.size() >1) {
            char current = heap.remove();
            sb.append(current);
            char next = heap.remove();
            sb.append(next);
            map.replace(current, map.get(current) -1);
            map.replace(next, map.get(next)-1);
            if(map.get(current) > 0) {
                heap.add(current);
            }
            if(map.get(next) > 0) {
                heap.add(next);
            }
        }
        if(!heap.isEmpty()){
            char lastChar = heap.remove();
            if(map.get(lastChar)>1) {
                return "";
            }
            sb.append(lastChar);
        }
        return sb.toString();
    }
}
