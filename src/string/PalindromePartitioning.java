package string;

import java.util.ArrayList;
import java.util.List;

import util.Print;

public class PalindromePartitioning {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PalindromePartitioning p = new PalindromePartitioning();
		Print.printDoubleList(p.partition("aabc"));
	}

	// backtracking 
	public List<List<String>> partition(String s) {
		List<List<String>> results = new ArrayList<>();
		addPalindrome(s, 0, new ArrayList<String>(), results);
		return results;
	}
	
	
	public void addPalindrome(String s, int start, List<String> partition, List<List<String>> results) {
		if(start == s.length()) {
			List<String> temp = new ArrayList<String>(partition);
			results.add(temp);
			return;
		}
		for(int i=start+1; i<=s.length(); i++) {
			String str = s.substring(start, i);
			if(isPalindorme(str)) {
				partition.add(str);
				addPalindrome(s, i, partition, results);
				partition.remove(partition.size()-1);
			}
		}
	}

	public boolean isPalindorme(String str) {
		StringBuilder sb = new StringBuilder();
		sb.append(str);
		return str.equals(sb.reverse().toString());
	}

}
