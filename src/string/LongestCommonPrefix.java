package string;

/*
 Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "
 */
public class LongestCommonPrefix {
	public static void main(String[] args) {
       String [] input = {"aca", "cba"};
       LongestCommonPrefix l = new LongestCommonPrefix();
       System.out.println(l.longestCommonPrefix(input));
	}

	// looping shorter length,  keep append matched char, end if it is not matched.
	public String longestCommonPrefix(String[] strs) {
        String cmnPrefix = strs[0];
		for(int i=1; i<strs.length; i++){
        	  cmnPrefix = longestPrefix(cmnPrefix,strs[i]);
        }
		return cmnPrefix;
	}
	
	public String longestPrefix(String a, String b) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<a.length() && i<b.length(); i++){
			if(a.charAt(i) == b.charAt(i)){
				sb.append(a.charAt(i));
			} else {
				break;
			}
		}
		return sb.toString();
	}
}
