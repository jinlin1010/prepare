package string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import util.Print;
 
/**
 * Input:
s: "abab" p: "ab"

Output:
[0, 1, 2]

Explanation:
The substring with start index = 0 is "ab", which is an anagram of "ab".
The substring with start index = 1 is "ba", which is an anagram of "ab".
The substring with start index = 2 is "ab", which is an anagram of "ab".
 * @author jinlin
 *
 */
public class FindAllAnagramsInAString {
	public static void main (String args []){
		String inputStr1 = "abab";
		String inputStr2 = "ab";
		FindAllAnagramsInAString f = new FindAllAnagramsInAString();
		Print.printIntegerList(f.findAnagrams(inputStr1, inputStr2));
	}
	
	// sliding window.
    public List<Integer> findAnagrams(String s, String p) {
        List<Integer> res = new ArrayList<>();
        if(s==null || p==null || s.length() == 0 || p.length()== 0) return res;
        int beginIndex = 0;
        int endIndex = beginIndex + p.length();
        while(endIndex <= s.length()){
            String str = s.substring(beginIndex, endIndex);
            if(isAnagrams(str,p)){
                res.add(beginIndex);
            }
            beginIndex++;
            endIndex++;
        }
        return res;
    }
    
    // bad way of doing isAnagrams time exceed.
    public boolean isAnagrams1(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        char[] c2 = s2.toCharArray();
        Arrays.sort(c1);
        Arrays.sort(c2);
        return String.valueOf(c1).equals(String.valueOf(c2));
    }
    
    //using char count int arr[‘a’]  => int arr[97]
    public boolean isAnagrams(String s1, String s2) {
    	int []arr1 = new int[256];
    	int []arr2 = new int[256];
    	for(int i=0; i<s1.length(); i++){
    		arr1[s1.charAt(i)]++;
    		arr2[s2.charAt(i)]++;
    	}
    	for (int i=0; i<arr1.length; i++) {
    		if(arr1[i] != arr2[i]) return false;
    	}
     	return true;
    }
}
