package string;

public class PalindromePartitioningII {
	public static void main (String args []) {
		PalindromePartitioningII pp = new PalindromePartitioningII();
		System.out.println(pp.minCut("abb"));
	}
	
	// looping,  右下角开始往上在往右
	// s[i] == s[j] && j-i <2 (相同或者相邻） && dp左下角==true   then 
	// 记载 dp == true,  a[i] = min(a[i], a[j+1]+1);
    public int minCut(String s) {
        if(s == null || s.length() <= 1) return 0;
        int strLen = s.length();
        boolean [][] dp = new boolean [strLen][strLen];
        int [] result = new int[strLen+1];
        for(int i=strLen-1; i>=0; i--) {
            result[i] = strLen - i;
            for(int j=i; j<strLen; j++) {
                if(s.charAt(i) == s.charAt(j)) {
                    if(j-i <2 || dp[i+1][j-1]) {
                        dp[i][j] = true;
                        result[i] = Math.min(result[i], result[j+1]+1);
                    }
                }
            }
        }
        return result[0]-1;
        
    }
}
