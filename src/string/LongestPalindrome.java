package string;

public class LongestPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = "babad";
        LongestPalindrome l = new LongestPalindrome();
		System.out.println(l.longestPalindrome(input));

	}

	public String longestPalindrome(String s) {
		if (s == null || s.length() < 0) {
			return "";
		}
		String result = "";
		int maxLength = 0;
		for (int i = 0; i < s.length()*2-1; i++) {
			int left = i / 2;
			int right = i / 2;
			if (i % 2 == 1)
				right++;
			String isPalindrome = lengthOfPalindrome(s, left, right);
			if (maxLength < isPalindrome.length()) {
				maxLength = isPalindrome.length();
				result = isPalindrome;
			}
		}

		return result;

	}

	public String lengthOfPalindrome(String s, int left, int right) {
		while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
			left--;
			right++;
		}
		return s.substring(left + 1, right);
	}
}