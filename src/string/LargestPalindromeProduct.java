package string;


/*
 Find the largest palindrome made from the product of two n-digit numbers.

Since the result could be very large, you should return the largest palindrome mod 1337.

 

Example:

Input: 2

Output: 987

Explanation: 99 x 91 = 9009, 9009 % 1337 = 987
 */
public class LargestPalindromeProduct {
	public static void main(String args[]) {
		LargestPalindromeProduct l = new LargestPalindromeProduct();
		System.out.println(l.largestPalindrome(2));
	}

	/* 
	 * n = 2,
	 * upperBound = 99,  lower = 10,  
	 * 99*99=9801,  find 98 palindrom 9889 > 9981 skip 
	 * 99*98=9702,  find 97 palindrom 9779    9779%i=!0 while (i between 99~10), skip 
	 * 99*91=9009,  find 90 palindrom 9009,   9009%99=91, 9009%1337=987 yes
	 * */
	public int largestPalindrome(int n) {
		if (n == 1)
			return 9;
		int upperBound = (int) Math.pow(10, n) - 1;
		int lowerBound = (int) upperBound / 10 + 1;
		long max = (long) upperBound * (long) upperBound;
		int half = (int) (max / (long) Math.pow(10, n));
		boolean foundPalindrome = false;
		long palindrome = 0;
		while (!foundPalindrome) {
		    palindrome = createPalindrome(half);
			for (long i = upperBound; i >= lowerBound; i--) {
				if (palindrome > i * i) {
					break;
				} else if (palindrome % i == 0) {
					System.out.println(i);
					foundPalindrome = true;
					break;
				}
			}
			half--;
		}
		System.out.println(palindrome);
		return (int) (palindrome % 1337);
	}

	public Long createPalindrome(long half) {
		StringBuilder sb = new StringBuilder();
		String reverse = half + sb.append(half).reverse().toString();
		return Long.parseLong(reverse);
	}
}
