package string;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class LongestStringChain {
	// Leetcode 1048
	public static void main(String args[]) {
		LongestStringChain l = new LongestStringChain();
		// System.out.println(l.isSubstring("bc", "bca"));
		// System.out.println(l.isSubstring("abca", "bc"));
		// System.out.println(l.isSubstring("bca", "abca"));
		String[] arr = {"sgtnz","sgtz","sgz","ikrcyoglz","ajelpkpx","ajelpkpxm","srqgtnz","srqgotnz","srgtnz","ijkrcyoglz"};
		String[] arr1 = {"ksqvsyq","ks","kss","czvh","zczpzvdhx","zczpzvh","zczpzvhx","zcpzvh","zczvh","gr","grukmj","ksqvsq","gruj","kssq","ksqsq","grukkmj","grukj","zczpzfvdhx","gru"};
		System.out.println(l.longestStrChain(arr1));
		System.out.println(l.longestStrChain(arr));


	}

	public int longestStrChain(String[] words) {
		if (words == null || words.length < 1)
			return 0;
		int len = words.length;
		int[] dp = new int[len];
		for(int i=0; i<dp.length; i++) {
			dp[i] = 1;
		}
		
		Arrays.sort(words, new Comparator<String>() {
		    @Override
		    public int compare(String o1, String o2) {
		        return Integer.compare(o1.length(), o2.length());
		    }
		});
		
		for (int i = 1; i < len; i++) {
			for (int j = 0; j < i; j++) {
				if (predecessor(words[j], words[i])) {
					System.out.println(words[j] + " " + words[i] + " " + predecessor(words[j], words[i]) + "" + dp[i]);

					dp[i] = Math.max(dp[i], dp[j] + 1);
					
				}
			}
		}
		int max =0;
		for(int i=0; i<dp.length; i++) {
			max = Math.max(max, dp[i]);
		}
		return max;
	}

	public boolean predecessor(String s1, String s2) {
		if (s2.length() - s1.length() != 1)
			return false;
		int[] c = new int[256];

		for (int i = 0; i < s1.length(); i++) {
			c[s1.charAt(i)]++;
		}
		for (int i = 0; i < s2.length(); i++) {
			c[s2.charAt(i)]--;
		}
		for (int i = 0; i < c.length; i++) {
			if (c[i] > 0)
				return false;
		}
		return true;
	}
}
