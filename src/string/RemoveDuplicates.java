package string;

public class RemoveDuplicates {

	public static void main(String[] args) {
		RemoveDuplicates r = new RemoveDuplicates();
		System.out.println(r.removeDuplicates("deeedbbcccbdaa", 3));

	}

	public String removeDuplicates(String s, int k) {
		if(s == "") return s;
        for(int i=0; i<s.length()-1; i++) {
          if(s.charAt(i) == s.charAt(i+1)) {
              int end = i + 1;
              while(end < s.length() && end-i< k && s.charAt(i) == s.charAt(end)){
                end++;  
              }
              if(end - i !=k) continue;
              s = s.substring(0, i) + s.substring(end, s.length());
              return removeDuplicates(s, k);
          }
        }
        return s;
	}

}
