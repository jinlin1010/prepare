package string;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class SmallestStringWithSwaps {
	private int[] roots;

	public static void main (String [] args) {
		
		List <Integer> p = Arrays.asList(2,3);
		List <Integer> p1 = Arrays.asList(1,3);
		List <Integer> p2 = Arrays.asList(1,2);
		List <Integer> p3 = Arrays.asList(2,3);
		List <Integer> p4 = Arrays.asList(2,4);
		List <Integer> p5 = Arrays.asList(0,1);
		List<List<Integer>> pairs = new ArrayList<>();
	//	pairs.add(p);
		pairs.add(p1);
//		pairs.add(p2);
//		pairs.add(p3);
//		pairs.add(p4);
		pairs.add(p5);
		SmallestStringWithSwaps s = new SmallestStringWithSwaps();
		System.out.println(s.smallestStringWithSwaps("dcaeb", pairs));
		
	}
	public String smallestStringWithSwaps(String s, List<List<Integer>> pairs) {
		if (s.length() == 0)
			return s;
		this.roots = new int[s.length()];
		for (int i = 0; i < s.length(); i++) {
			roots[i] = i;
		}
		for (List<Integer> pair : pairs) {
			int a = pair.get(0);
			int b = pair.get(1);
			System.out.println(find(a));
			System.out.println(find(b));
			if (find(a) != find(b)) {
				union(a, b);
			}
		}
		for(int i=0; i<roots.length; i++) {
			System.out.print(roots[i]);
		}
		Map<Integer, PriorityQueue<Character>> map = new HashMap<>();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			int root = find(i);
			if (!map.containsKey(root)) {
				map.put(root, new PriorityQueue<>());
			}
			map.get(root).add(c);
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			int root = find(i);
			sb.append(map.get(root).poll());
		}
		return sb.toString();
	}

	private int find(int v) {
		if (roots[v] == v) {
			return v;
		}
		roots[v] = find(roots[v]);
		return roots[v];
	}

	private void union(int a, int b) {
		int roota = find(a);
		int rootb = find(b);
		if (roota != rootb) {
			roots[roota] = rootb;
		}
	}
}
