package string;

/*
Given a string s, find the longest palindromic subsequence's length in s. You may assume that the maximum length of s is 1000.

Example 1:
Input:

"bbbab"
Output:
4
One possible longest palindromic subsequence is "bbbb".
Example 2:
Input:

"cbbd"
Output:
2
 * 
 * 
 */
public class LongestPalindromeSubseq {
	public static void main (String args[]) {
		LongestPalindromeSubseq l = new LongestPalindromeSubseq();
		System.out.println(l.longestPalindromeSubseq("bbbab"));
	}

	//dp 建模，  if(s.charAt(i)==s.charAt(j)) dp[i][j]=左下角+2， else 比较max(左边，下面），
    public int longestPalindromeSubseq(String s) {
        if(s==null || s.length() == 0) return 0;
        int n = s.length();
        int [][] dp = new int [n][n];
        for(int i=n-1; i>=0; i--){
            dp [i][i] = 1;
            for(int j=i+1; j<n; j++) {
                if(s.charAt(i) == s.charAt(j)) {
                    dp[i][j] = dp[i+1][j-1] + 2;
                } else {
                    dp[i][j] = Integer.max(dp[i][j-1], dp[i+1][j]);
                }
            }
        }
        return dp[0][n-1];
    }
}
