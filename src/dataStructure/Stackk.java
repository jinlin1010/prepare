package dataStructure;

import java.util.ArrayDeque;
import java.util.Deque;

import util.Print;

public class Stackk {

	public int[] dailyTemperatures(int[] temperatures) {
		if (temperatures == null)
			return null;
		int[] res = new int[temperatures.length];
		Deque<Integer> stack = new ArrayDeque<>();
		for (int i = temperatures.length - 1; i >= 0; i--) {
			while (!stack.isEmpty() && temperatures[stack.peek()] <= temperatures[i]) {
				stack.pop();
			}

			res[i] = stack.isEmpty() ? 0 : stack.peek() - i;
			stack.push(i);
		}
		return res;
	}

	/*
	 * 1. 输入是一个数组，代表每个人的身高。有一块屏幕，在这一排人的右侧、如果前面有人身高比自己高，这个人就看不到屏幕。 问哪些人能看到屏幕，输出对应坐标。
	 * 2. 增加条件，对第i个人，只要在他前面的个位置内没有比他高的，i就能看到屏幕。问哪些人能看到屏幕，输出对应坐标。 3.
	 * 假如说所有人身高可能的取值只有k个，且k<<d，问有没有更好的解法
	 */

	// hint: always keep the max in the first index of deque
	public int[] maxSlidingWindow(int[] nums, int k) {
		int[] max = new int[nums.length - k + 1];
		Deque<Integer> ds = new ArrayDeque<>();
		for (int i = 0; i < k; i++) {
			while (!ds.isEmpty() && nums[ds.peekLast()] < nums[i])
				ds.pollLast();
			ds.offerLast(i);
		}
		int j = 0;
		max[j++] = nums[ds.peekFirst()];
		for (int i = k; i < nums.length; i++) {
			if (ds.peekFirst() == i - k)
				ds.pollFirst();
			while (!ds.isEmpty() && nums[ds.peekLast()] < nums[i])
				ds.pollLast();
			ds.offerLast(i);
			max[j++] = nums[ds.peekFirst()];
		}
		return max;
	}

	public static void main(String[] args) {
		Stackk s = new Stackk();
		int[] nums = { 1, 3, -1, -3, 5, 3, 6, 7 };
		int k = 3;
		Print.printArray(s.maxSlidingWindow(nums, k));
		// System.out.println(s.maxSlidingWindow(nums, k)) ;
	}
}
