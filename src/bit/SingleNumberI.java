package bit;

public class SingleNumberI {

	public static void main (String args []) {
		int [] arr = {2,2,1,3,3,4,4};
		SingleNumberI s = new SingleNumberI();
		System.out.println(s.singleNumber(arr));
	}
	
	
	public int singleNumber(int [] A) {
		int res = 0;
		for(int i=0; i<A.length; i++) {
			res ^= A[i];
		}
		return res;
	}
}
