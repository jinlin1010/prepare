package bit;

import util.Print;

public class SingleNumberIII {
    // find two number that only occur once
	public static void main(String[] args) {
		SingleNumberIII s = new SingleNumberIII();
		int [] arr = {1,2,1,3,2,5};
		Print.printArray(s.singleNumber(arr));

	}
	
	public int[] singleNumber(int[] nums) {
		int result=0;
		for(int i=0; i<nums.length; i++)
			result^=nums[i];
    
		int bitlocation=1;
		int temp=result;
		for(int i=0; i<32; i++){
			if((temp&1)==1)
				break;
			bitlocation<<=1;
			temp>>=1;
		}
    
		int result1=0;
		for(int i=0; i<nums.length; i++) {
			if((nums[i]&bitlocation)>0)
				result1^=nums[i];
		}
    
		return new int[]{result1, result^result1};
	}

}
