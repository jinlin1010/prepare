package bit;

public class SingleNumberII {

	public static void main(String[] args) {
		int [] arr = {2,2,3,2};
		SingleNumberII s = new SingleNumberII();
		System.out.println(s.singleNumber(arr));
		System.out.println(s.getSingle(arr));

	}
	
	public int singleNumber(int[] A) {
		int x =0;
		int y = 0;
		for(int i=0; i<A.length; i++) {
			int x1 = ( A[i] ^ x) & ~y;
			int y1 = (A[i] & x ) | (~A[i] & y);
			x = x1;
			y = y1;
		}
		return x;
	}
	
	public int getSingle(int [] A) {
		int ones = 0;
		int twos = 0;
		int common_bit_mask;
		for(int i=0; i<A.length; i++) {
			twos = twos | (ones & A[i]);
			ones = ones ^ A[i];
			common_bit_mask = ~(ones & twos);
			ones &= common_bit_mask;
			twos &= common_bit_mask;
		}
		return ones;
	}

}
